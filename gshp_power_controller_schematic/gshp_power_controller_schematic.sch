EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date "2020-11-24"
Rev "0.9"
Comp "Kasetsart University"
Comment1 "Vasutorn S."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CircuitBreaker_2P CB1
U 1 1 5FBCA0C9
P 1450 1100
F 0 "CB1" V 1050 1200 50  0000 R CNN
F 1 "CircuitBreaker_2P" V 1150 1450 50  0000 R CNN
F 2 "" H 1350 1100 50  0001 C CNN
F 3 "~" H 1350 1100 50  0001 C CNN
	1    1450 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	650  1200 1150 1200
Wire Wire Line
	650  1000 1150 1000
Text GLabel 650  1000 0    50   Input ~ 0
L
Text GLabel 650  1200 0    50   Input ~ 0
N
Wire Wire Line
	5500 1200 5500 2850
$Comp
L Device:CircuitBreaker_2P CB4
U 1 1 5FBCD1A9
P 6200 1100
F 0 "CB4" V 5835 1100 50  0000 C CNN
F 1 "CB_AIR_C_REF" V 5926 1100 50  0000 C CNN
F 2 "" H 6100 1100 50  0001 C CNN
F 3 "~" H 6100 1100 50  0001 C CNN
	1    6200 1100
	0    1    1    0   
$EndComp
$Comp
L Device:CircuitBreaker_2P CB5
U 1 1 5FBCE8AA
P 6200 2750
F 0 "CB5" V 5835 2750 50  0000 C CNN
F 1 "CB_AIR_C" V 5926 2750 50  0000 C CNN
F 2 "" H 6100 2750 50  0001 C CNN
F 3 "~" H 6100 2750 50  0001 C CNN
	1    6200 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 1000 5250 2650
Wire Wire Line
	5250 2650 5900 2650
Wire Wire Line
	5500 2850 5900 2850
$Comp
L Device:CircuitBreaker_2P CB7
U 1 1 5FBCFAF6
P 6200 4450
F 0 "CB7" V 5835 4450 50  0000 C CNN
F 1 "CB_WATER_PUMP" V 5926 4450 50  0000 C CNN
F 2 "" H 6100 4450 50  0001 C CNN
F 3 "~" H 6100 4450 50  0001 C CNN
	1    6200 4450
	0    1    1    0   
$EndComp
$Comp
L Device:CircuitBreaker_2P CB3
U 1 1 5FBD0078
P 2600 3350
F 0 "CB3" V 2235 3350 50  0000 C CNN
F 1 "CB_MEASUREMENT" V 2326 3350 50  0000 C CNN
F 2 "" H 2500 3350 50  0001 C CNN
F 3 "~" H 2500 3350 50  0001 C CNN
	1    2600 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 1900 2300 1900
Wire Wire Line
	1950 1700 2300 1700
Wire Wire Line
	6500 2650 7750 2650
$Comp
L Device:CircuitBreaker_2P CB2
U 1 1 5FBE4530
P 2600 1800
F 0 "CB2" V 2235 1800 50  0000 C CNN
F 1 "CB_Auxiliary" V 2326 1800 50  0000 C CNN
F 2 "" H 2500 1800 50  0001 C CNN
F 3 "~" H 2500 1800 50  0001 C CNN
	1    2600 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 3450 2300 3450
Wire Wire Line
	1950 3250 2300 3250
Wire Wire Line
	2900 1700 3350 1700
Wire Wire Line
	2900 1900 3350 1900
$Comp
L Connector:Conn_WallSocket J1
U 1 1 5FBE67C1
P 3550 1800
F 0 "J1" H 3704 1846 50  0000 L CNN
F 1 "Conn_WallSocket" H 3704 1755 50  0000 L CNN
F 2 "" H 3150 1800 50  0001 C CNN
F 3 "~" H 3150 1800 50  0001 C CNN
	1    3550 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1000 7750 1000
$Comp
L gshp_power_controller_schematic-rescue:CDU_Fan-gshp_lib U11
U 1 1 5FBF808F
P 10400 3850
F 0 "U11" H 10778 3796 50  0000 L CNN
F 1 "CDU_Fan" H 10350 3750 50  0000 L CNN
F 2 "" H 10400 3850 50  0001 C CNN
F 3 "" H 10400 3850 50  0001 C CNN
	1    10400 3850
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CDU_Com-gshp_lib U13
U 1 1 5FC007E8
P 10400 3400
F 0 "U13" H 10778 3346 50  0000 L CNN
F 1 "CDU_Com" H 10350 3300 50  0000 L CNN
F 2 "" H 10400 3400 50  0001 C CNN
F 3 "" H 10400 3400 50  0001 C CNN
	1    10400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4350 7750 4350
$Comp
L gshp_power_controller_schematic-rescue:FCU-gshp_lib U10
U 1 1 5FC146BA
P 10400 1000
F 0 "U10" H 10628 951 50  0000 L CNN
F 1 "FCU" H 10350 900 50  0000 L CNN
F 2 "" H 10400 1000 50  0001 C CNN
F 3 "" H 10400 1000 50  0001 C CNN
	1    10400 1000
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CDU_Fan-gshp_lib U9
U 1 1 5FC168A7
P 10400 2200
F 0 "U9" H 10778 2146 50  0000 L CNN
F 1 "CDU_Fan" H 10350 2100 50  0000 L CNN
F 2 "" H 10400 2200 50  0001 C CNN
F 3 "" H 10400 2200 50  0001 C CNN
	1    10400 2200
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CDU_Com-gshp_lib U8
U 1 1 5FC18C80
P 10400 1750
F 0 "U8" H 10778 1696 50  0000 L CNN
F 1 "CDU_Com" H 10350 1650 50  0000 L CNN
F 2 "" H 10400 1750 50  0001 C CNN
F 3 "" H 10400 1750 50  0001 C CNN
	1    10400 1750
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:FCU-gshp_lib U14
U 1 1 5FC410E2
P 10400 2650
F 0 "U14" H 10800 2650 50  0000 L CNN
F 1 "FCU" H 10350 2550 50  0000 L CNN
F 2 "" H 10400 2650 50  0001 C CNN
F 3 "" H 10400 2650 50  0001 C CNN
	1    10400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 3250 1950 3700
Wire Wire Line
	1950 3700 3050 3700
Connection ~ 1950 3250
Wire Wire Line
	1950 3700 1950 3800
Wire Wire Line
	1950 3800 3050 3800
Connection ~ 1950 3700
Wire Wire Line
	1950 3800 1950 3900
Wire Wire Line
	1950 3900 3050 3900
Connection ~ 1950 3800
Wire Wire Line
	3050 3450 2900 3450
Wire Wire Line
	2900 3250 3050 3250
Wire Wire Line
	4150 6400 4450 6400
Wire Wire Line
	4550 6200 4150 6200
Wire Wire Line
	4150 6300 4450 6300
Wire Wire Line
	4450 6300 4450 6400
Wire Wire Line
	4450 6400 9850 6400
Wire Wire Line
	4550 6300 9850 6300
Wire Wire Line
	4550 6300 4550 6200
Connection ~ 4450 6400
$Comp
L gshp_power_controller_schematic-rescue:FD-Q32C-gshp_lib U15
U 1 1 5FCACA45
P 10150 5900
F 0 "U15" H 10578 5596 50  0000 L CNN
F 1 "FD-Q32C" H 10578 5505 50  0000 L CNN
F 2 "" H 10150 5900 50  0001 C CNN
F 3 "" H 10150 5900 50  0001 C CNN
	1    10150 5900
	1    0    0    -1  
$EndComp
NoConn ~ 9100 6200
Wire Wire Line
	1750 1200 2150 1200
Wire Wire Line
	1750 1000 1950 1000
Wire Wire Line
	4150 6100 9850 6100
Wire Wire Line
	1950 1700 1950 1000
Connection ~ 1950 1700
Wire Wire Line
	2150 1200 2150 1900
Connection ~ 2150 1900
$Comp
L gshp_power_controller_schematic-rescue:PT100_Sensor-gshp_lib U17
U 1 1 5FD0B1FD
P 10200 5450
F 0 "U17" H 10428 5201 50  0000 L CNN
F 1 "PT100_Sensor" H 10428 5110 50  0000 L CNN
F 2 "" H 10200 5450 50  0001 C CNN
F 3 "" H 10200 5450 50  0001 C CNN
	1    10200 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 5600 4150 5600
Wire Wire Line
	9850 5700 4150 5700
Wire Wire Line
	9850 5800 4150 5800
Wire Wire Line
	9850 5900 4150 5900
Wire Wire Line
	5250 1000 5900 1000
Wire Wire Line
	5500 1200 5900 1200
$Comp
L gshp_power_controller_schematic-rescue:PT100_Sensor-gshp_lib U16
U 1 1 5FD1D5CC
P 10200 4900
F 0 "U16" H 10428 4651 50  0000 L CNN
F 1 "PT100_Sensor" H 10428 4560 50  0000 L CNN
F 2 "" H 10200 4900 50  0001 C CNN
F 3 "" H 10200 4900 50  0001 C CNN
	1    10200 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 5050 9850 5050
Wire Wire Line
	4150 5150 9850 5150
Wire Wire Line
	4150 5250 9850 5250
Wire Wire Line
	9850 5350 4150 5350
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U1
U 1 1 5FD56178
P 7950 750
F 0 "U1" H 7950 756 50  0000 C CNN
F 1 "P_AC_REF" H 7950 665 50  0000 C CNN
F 2 "" H 7950 750 50  0001 C CNN
F 3 "" H 7950 750 50  0001 C CNN
	1    7950 750 
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U6
U 1 1 5FD61154
P 7950 4100
F 0 "U6" H 7950 4106 50  0000 C CNN
F 1 "P_WATER_PUMP" H 7950 4015 50  0000 C CNN
F 2 "" H 7950 4100 50  0001 C CNN
F 3 "" H 7950 4100 50  0001 C CNN
	1    7950 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 4350 10100 4350
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U3
U 1 1 5FD5FCAA
P 7950 1500
F 0 "U3" H 7950 1506 50  0000 C CNN
F 1 "P_CDU_COM_REF" H 7950 1415 50  0000 C CNN
F 2 "" H 7950 1500 50  0001 C CNN
F 3 "" H 7950 1500 50  0001 C CNN
	1    7950 1500
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U5
U 1 1 5FD608A1
P 7950 3600
F 0 "U5" H 7950 3606 50  0000 C CNN
F 1 "P_CDU_FAN_TEST" H 7950 3515 50  0000 C CNN
F 2 "" H 7950 3600 50  0001 C CNN
F 3 "" H 7950 3600 50  0001 C CNN
	1    7950 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3850 7750 3850
Wire Wire Line
	8150 3850 10100 3850
Wire Wire Line
	8150 3400 10100 3400
$Comp
L gshp_power_controller_schematic-rescue:WaterPump-gshp_lib U12
U 1 1 5FC3E916
P 10400 4350
F 0 "U12" H 10778 4296 50  0000 L CNN
F 1 "WaterPump" H 10300 4250 50  0000 L CNN
F 2 "" H 10400 4350 50  0001 C CNN
F 3 "" H 10400 4350 50  0001 C CNN
	1    10400 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4550 10100 4550
Wire Notes Line width 20 style solid rgb(255, 0, 0)
	9400 6800 1000 6800
Wire Notes Line width 20 style solid rgb(255, 0, 0)
	1000 6800 1000 600 
Wire Wire Line
	1950 1700 1950 3250
Wire Wire Line
	2150 1900 2150 3450
Text Notes 10550 6100 0    50   Italic 10
Flow Meter
Wire Wire Line
	8150 1000 10100 1000
$Sheet
S 3050 3050 1100 3500
U 5FC493CE
F0 "gshp_measurement_system" 50
F1 "gshp_measurement_system.sch" 50
F2 "Ua" I L 3050 3700 50 
F3 "Ub" I L 3050 3800 50 
F4 "Uc" I L 3050 3900 50 
F5 "N" I L 3050 3450 50 
F6 "L" I L 3050 3250 50 
F7 "PT100_A_1" I R 4150 5050 50 
F8 "PT100_B_1" I R 4150 5250 50 
F9 "+I_Flow" I R 4150 6200 50 
F10 "-I_Flow" I R 4150 6300 50 
F11 "PT100_Aa_1" I R 4150 5150 50 
F12 "PT100_Bb_1" I R 4150 5350 50 
F13 "PT100_Bb_2" I R 4150 5900 50 
F14 "PT100_Aa_2" I R 4150 5700 50 
F15 "PT100_A_2" I R 4150 5600 50 
F16 "PT100_B_2" I R 4150 5800 50 
F17 "+24V" I R 4150 6100 50 
F18 "GNDA" I R 4150 6400 50 
F19 "CT_1_K" I R 4150 3150 50 
F20 "CT_2_K" I R 4150 3400 50 
F21 "CT_3_K" I R 4150 3650 50 
F22 "CT_4_K" I R 4150 3900 50 
F23 "CT_5_K" I R 4150 4150 50 
F24 "CT_6_K" I R 4150 4400 50 
F25 "CT_7_K" I R 4150 4650 50 
F26 "CT_1_L" I R 4150 3250 50 
F27 "CT_2_L" I R 4150 3500 50 
F28 "CT_3_L" I R 4150 3750 50 
F29 "CT_4_L" I R 4150 4000 50 
F30 "CT_5_L" I R 4150 4250 50 
F31 "CT_6_L" I R 4150 4500 50 
F32 "CT_7_L" I R 4150 4750 50 
$EndSheet
Wire Wire Line
	9850 1950 10100 1950
Connection ~ 9850 1950
Wire Wire Line
	10100 2400 9850 2400
Wire Wire Line
	9850 1950 9850 2400
Wire Wire Line
	6550 1350 6550 1750
Wire Wire Line
	6550 1750 7750 1750
Wire Wire Line
	6550 2200 6550 1750
Wire Wire Line
	6550 2200 7750 2200
Connection ~ 6550 1750
Wire Wire Line
	8150 1750 10100 1750
Wire Wire Line
	8150 2200 10100 2200
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U4
U 1 1 5FD60435
P 7950 1950
F 0 "U4" H 7950 1956 50  0000 C CNN
F 1 "P_CDU_FAN_REF" H 7950 1865 50  0000 C CNN
F 2 "" H 7950 1950 50  0001 C CNN
F 3 "" H 7950 1950 50  0001 C CNN
	1    7950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1350 10100 1350
Wire Wire Line
	10100 2650 8150 2650
Wire Wire Line
	10100 3000 6550 3000
Wire Wire Line
	6550 3400 7750 3400
Wire Wire Line
	9850 3600 10100 3600
Wire Wire Line
	9850 3200 10100 3200
Wire Wire Line
	9850 3200 9850 3600
Connection ~ 5500 2850
Connection ~ 5250 2650
Wire Wire Line
	5900 4350 5250 4350
Wire Wire Line
	5500 4550 5900 4550
Wire Notes Line width 20 style solid rgb(255, 0, 0)
	1000 600  9400 600 
Wire Wire Line
	1950 1000 5250 1000
Connection ~ 1950 1000
Connection ~ 5250 1000
Wire Wire Line
	2150 1200 5500 1200
Connection ~ 2150 1200
Connection ~ 5500 1200
Wire Notes Line width 20 style solid rgb(255, 0, 0)
	9400 600  9400 6800
Wire Wire Line
	9100 6200 9850 6200
Wire Wire Line
	5250 2650 5250 4350
$Comp
L Device:CircuitBreaker_2P CB6
U 1 1 5FBCF608
P 7200 3950
F 0 "CB6" V 6835 3950 50  0000 C CNN
F 1 "CB_CDU_FAN" V 6926 3950 50  0000 C CNN
F 2 "" H 7100 3950 50  0001 C CNN
F 3 "~" H 7100 3950 50  0001 C CNN
	1    7200 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 3850 6550 3850
Wire Wire Line
	6550 3000 6550 3400
Connection ~ 6550 3400
Wire Wire Line
	6550 3400 6550 3850
Wire Wire Line
	4150 3150 4350 3150
Text HLabel 4350 3400 2    50   Input ~ 0
CT_2_K
Text HLabel 4350 3500 2    50   Input ~ 0
CT_2_L
Text HLabel 4350 3650 2    50   Input ~ 0
CT_3_K
Text HLabel 4350 3750 2    50   Input ~ 0
CT_3_L
Text HLabel 4350 3900 2    50   Input ~ 0
CT_4_K
Text HLabel 4350 4000 2    50   Input ~ 0
CT_4_L
Text HLabel 4350 4150 2    50   Input ~ 0
CT_5_K
Text HLabel 4350 4250 2    50   Input ~ 0
CT_5_L
Text HLabel 4350 4400 2    50   Input ~ 0
CT_6_K
Text HLabel 4350 4500 2    50   Input ~ 0
CT_6_L
Text HLabel 4350 4650 2    50   Input ~ 0
CT_7_K
Text HLabel 4350 4750 2    50   Input ~ 0
CT_7_L
Wire Wire Line
	4350 4650 4150 4650
Wire Wire Line
	4350 4750 4150 4750
Wire Wire Line
	4350 4400 4150 4400
Wire Wire Line
	4350 4500 4150 4500
Wire Wire Line
	4150 4250 4350 4250
Wire Wire Line
	4350 4150 4150 4150
Wire Wire Line
	4350 4000 4150 4000
Wire Wire Line
	4350 3900 4150 3900
Wire Wire Line
	4350 3750 4150 3750
Wire Wire Line
	4350 3500 4150 3500
Wire Wire Line
	4150 3650 4350 3650
Wire Wire Line
	4350 3400 4150 3400
Wire Wire Line
	4150 3250 4350 3250
Text HLabel 4350 3250 2    50   Input ~ 0
CT_1_L
Text HLabel 4350 3150 2    50   Input ~ 0
CT_1_K
Text HLabel 7900 2750 0    50   Input ~ 0
CT_2_K
Text HLabel 8000 2750 2    50   Input ~ 0
CT_2_L
Text HLabel 7900 1850 0    50   Input ~ 0
CT_3_K
Text HLabel 8000 1850 2    50   Input ~ 0
CT_3_L
Text HLabel 7900 2300 0    50   Input ~ 0
CT_4_K
Text HLabel 8000 2300 2    50   Input ~ 0
CT_4_L
Text HLabel 7900 3950 0    50   Input ~ 0
CT_5_K
Text HLabel 8000 3950 2    50   Input ~ 0
CT_5_L
Text HLabel 7900 4450 0    50   Input ~ 0
CT_6_K
Text HLabel 8000 4450 2    50   Input ~ 0
CT_6_L
Text HLabel 7900 3500 0    50   Input ~ 0
CT_7_K
Text HLabel 8000 3500 2    50   Input ~ 0
CT_7_L
Text HLabel 8000 1100 2    50   Input ~ 0
CT_1_L
Text HLabel 7900 1100 0    50   Input ~ 0
CT_1_K
Wire Wire Line
	8000 4450 8000 4400
Wire Wire Line
	7900 4450 7900 4400
Wire Wire Line
	7900 3950 7900 3900
Wire Wire Line
	8000 3950 8000 3900
Wire Wire Line
	7900 3500 7900 3450
Wire Wire Line
	8000 3500 8000 3450
Wire Wire Line
	7900 2750 7900 2700
Wire Wire Line
	8000 2750 8000 2700
Wire Wire Line
	7900 2300 7900 2250
Wire Wire Line
	8000 2300 8000 2250
Wire Wire Line
	7900 1850 7900 1800
Wire Wire Line
	8000 1850 8000 1800
Wire Wire Line
	8000 1100 8000 1050
Wire Wire Line
	7900 1050 7900 1100
Wire Notes Line width 12 style solid
	9200 900  9600 900 
Wire Notes Line width 12 style solid
	9600 900  9600 2350
Wire Notes Line width 12 style solid
	9600 2350 9200 2350
Wire Notes Line width 12 style solid
	9200 2350 9200 900 
Wire Notes Line width 12 style solid
	9200 2550 9600 2550
Wire Notes Line width 12 style solid
	9600 2550 9600 4650
Wire Notes Line width 12 style solid
	9600 4650 9200 4650
Wire Notes Line width 12 style solid
	9200 4650 9200 2550
Text Notes 1050 6700 0    197  Italic 39
Enclosure
Text Notes 10450 5600 0    50   Italic 10
Temperature Sensor
Text Notes 10450 5050 0    50   Italic 10
Temperature Sensor
Wire Notes Line
	9600 5000 9600 5400
Wire Notes Line
	9600 5400 9200 5400
Wire Notes Line
	9200 5400 9200 5000
Wire Notes Line
	9200 5000 9600 5000
Wire Notes Line
	9200 5550 9600 5550
Wire Notes Line
	9600 5550 9600 5950
Wire Notes Line
	9600 5950 9200 5950
Wire Notes Line
	9200 5950 9200 5550
Wire Notes Line
	9200 6050 9600 6050
Wire Notes Line
	9600 6050 9600 6450
Wire Notes Line
	9600 6450 9200 6450
Wire Notes Line
	9200 6450 9200 6050
Wire Wire Line
	6500 2850 10100 2850
Wire Wire Line
	9850 3200 6750 3200
Connection ~ 9850 3200
Wire Wire Line
	5500 2850 5500 4550
Wire Wire Line
	6900 4050 6750 4050
Wire Wire Line
	6750 4050 6750 3200
Wire Wire Line
	7500 4050 10100 4050
Text Notes 9450 5000 0    50   ~ 0
TEMP_1
Text Notes 9450 5550 0    50   ~ 0
TEMP_2
Text Notes 9450 6050 0    50   ~ 0
FLOW
Text Label 8550 4350 0    50   ~ 0
L_WATER_PUMP
Text Label 8550 4550 0    50   ~ 0
N_WATER_PUMP
Text Label 8550 4050 0    50   ~ 0
N_CDU_FAN
Text Label 8550 3850 0    50   ~ 0
L_CDU_FAN
Text Label 8550 3400 0    50   ~ 0
L_CDU_COM
Text Label 8550 3200 0    50   ~ 0
N_CDU
Text Label 8550 3000 0    50   ~ 0
L_CDU
Text Label 8550 2650 0    50   ~ 0
L_AIR_C
Text Label 8550 2850 0    50   ~ 0
N_AIR_C
Text Label 8550 2200 0    50   ~ 0
L_CDU_FAN_REF
Text Label 8550 1750 0    50   ~ 0
L_CDU_COM_REF
Text Label 8550 1350 0    50   ~ 0
L_CDU_REF
Wire Wire Line
	10100 1550 9850 1550
Wire Wire Line
	9850 1550 9850 1950
Wire Wire Line
	6500 1200 10100 1200
Text Label 8550 1200 0    50   ~ 0
N_AIR_C_REF
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U7
U 1 1 5FD64426
P 7950 3150
F 0 "U7" H 7950 3156 50  0000 C CNN
F 1 "P_CDU_COM_TEST" H 7950 3065 50  0000 C CNN
F 2 "" H 7950 3150 50  0001 C CNN
F 3 "" H 7950 3150 50  0001 C CNN
	1    7950 3150
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:CT_Probe-gshp_lib U2
U 1 1 5FD648EE
P 7950 2400
F 0 "U2" H 7950 2406 50  0000 C CNN
F 1 "P_AC_TEST" H 7950 2315 50  0000 C CNN
F 2 "" H 7950 2400 50  0001 C CNN
F 3 "" H 7950 2400 50  0001 C CNN
	1    7950 2400
	1    0    0    -1  
$EndComp
Text Label 8550 1000 0    50   ~ 0
L_AIR_C_REF
$EndSCHEMATC
