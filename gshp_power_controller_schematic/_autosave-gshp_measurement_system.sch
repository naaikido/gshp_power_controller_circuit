EESchema Schematic File Version 5
EELAYER 36 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 2000 2550
Connection ~ 2200 2750
Connection ~ 3500 3400
Connection ~ 3500 3600
Connection ~ 7950 4050
Connection ~ 8050 4450
Wire Wire Line
	1750 800  6950 800 
Wire Wire Line
	1750 950  7050 950 
Wire Wire Line
	1750 1100 7150 1100
Wire Wire Line
	1750 1700 2000 1700
Wire Wire Line
	1750 1900 2200 1900
Wire Wire Line
	2000 2550 2000 1700
Wire Wire Line
	2000 2550 2000 3400
Wire Wire Line
	2000 3400 2600 3400
Wire Wire Line
	2200 1900 2200 2750
Wire Wire Line
	2200 2750 2200 3600
Wire Wire Line
	2200 2750 2600 2750
Wire Wire Line
	2200 3600 2600 3600
Wire Wire Line
	2600 2550 2000 2550
Wire Wire Line
	3300 2750 3500 2750
Wire Wire Line
	3500 2500 3500 2550
Wire Wire Line
	3500 2550 3300 2550
Wire Wire Line
	3500 2750 3500 2800
Wire Wire Line
	3500 3350 3500 3400
Wire Wire Line
	3500 3400 3300 3400
Wire Wire Line
	3500 3400 3650 3400
Wire Wire Line
	3500 3600 3300 3600
Wire Wire Line
	3500 3650 3500 3600
Wire Wire Line
	3650 3600 3500 3600
Wire Wire Line
	6050 3850 6050 4300
Wire Wire Line
	6050 3850 8800 3850
Wire Wire Line
	6050 4300 6650 4300
Wire Wire Line
	6250 3650 6250 4100
Wire Wire Line
	6250 3650 9000 3650
Wire Wire Line
	6250 4100 6650 4100
Wire Wire Line
	6450 1250 6450 1900
Wire Wire Line
	6450 1900 6650 1900
Wire Wire Line
	6650 3050 6550 3050
Wire Wire Line
	6950 800  6950 1350
Wire Wire Line
	7050 950  7050 1350
Wire Wire Line
	7150 1100 7150 1350
Wire Wire Line
	7250 1250 6450 1250
Wire Wire Line
	7250 1350 7250 1250
Wire Wire Line
	7650 1900 8400 1900
Wire Wire Line
	7650 2000 8400 2000
Wire Wire Line
	7650 2150 8400 2150
Wire Wire Line
	7650 2250 8400 2250
Wire Wire Line
	7650 2400 8400 2400
Wire Wire Line
	7650 2500 8400 2500
Wire Wire Line
	7650 2650 8400 2650
Wire Wire Line
	7650 2750 8400 2750
Wire Wire Line
	7650 2900 8400 2900
Wire Wire Line
	7650 3000 8400 3000
Wire Wire Line
	7650 3150 8400 3150
Wire Wire Line
	7650 3250 8400 3250
Wire Wire Line
	7650 4450 8050 4450
Wire Wire Line
	7650 4550 8200 4550
Wire Wire Line
	7650 4650 8200 4650
Wire Wire Line
	7650 4950 8100 4950
Wire Wire Line
	7950 3950 7950 4050
Wire Wire Line
	7950 4050 7650 4050
Wire Wire Line
	8050 4350 8050 4450
Wire Wire Line
	8050 4450 8200 4450
Wire Wire Line
	8100 3950 7950 3950
Wire Wire Line
	8100 4050 7950 4050
Wire Wire Line
	8100 4150 7650 4150
Wire Wire Line
	8100 4250 7650 4250
Wire Wire Line
	8100 4850 7650 4850
Wire Wire Line
	8200 4350 8050 4350
Wire Wire Line
	8400 1650 7650 1650
Wire Wire Line
	8400 1750 7650 1750
Wire Wire Line
	8750 5100 8750 5850
Wire Wire Line
	8750 5100 9150 5100
Wire Wire Line
	8800 3850 8800 4300
Wire Wire Line
	8800 4300 9150 4300
Wire Wire Line
	8850 5200 8850 5950
Wire Wire Line
	8850 5200 9150 5200
Wire Wire Line
	9000 3650 9000 4100
Wire Wire Line
	9000 4100 9150 4100
Text HLabel 1750 800  0    50   Input ~ 0
Ua
Text HLabel 1750 950  0    50   Input ~ 0
Ub
Text HLabel 1750 1100 0    50   Input ~ 0
Uc
Text HLabel 1750 1700 0    50   Input ~ 0
L
Text HLabel 1750 1900 0    50   Input ~ 0
N
Text HLabel 3650 3400 2    50   Input ~ 0
+24V
Text HLabel 3650 3600 2    50   Input ~ 0
GNDA
Text HLabel 8100 3950 2    50   Input ~ 0
PT100_A_1
Text HLabel 8100 4050 2    50   Input ~ 0
PT100_Aa_1
Text HLabel 8100 4150 2    50   Input ~ 0
PT100_B_1
Text HLabel 8100 4250 2    50   Input ~ 0
PT100_Bb_1
Text HLabel 8100 4850 2    50   Input ~ 0
+I_Flow
Text HLabel 8100 4950 2    50   Input ~ 0
-I_Flow
Text HLabel 8200 4350 2    50   Input ~ 0
PT100_A_2
Text HLabel 8200 4450 2    50   Input ~ 0
PT100_Aa_2
Text HLabel 8200 4550 2    50   Input ~ 0
PT100_B_2
Text HLabel 8200 4650 2    50   Input ~ 0
PT100_Bb_2
Text HLabel 8400 1650 2    50   Input ~ 0
CT_1_K
Text HLabel 8400 1750 2    50   Input ~ 0
CT_1_L
Text HLabel 8400 1900 2    50   Input ~ 0
CT_2_K
Text HLabel 8400 2000 2    50   Input ~ 0
CT_2_L
Text HLabel 8400 2150 2    50   Input ~ 0
CT_3_K
Text HLabel 8400 2250 2    50   Input ~ 0
CT_3_L
Text HLabel 8400 2400 2    50   Input ~ 0
CT_4_K
Text HLabel 8400 2500 2    50   Input ~ 0
CT_4_L
Text HLabel 8400 2650 2    50   Input ~ 0
CT_5_K
Text HLabel 8400 2750 2    50   Input ~ 0
CT_5_L
Text HLabel 8400 2900 2    50   Input ~ 0
CT_6_K
Text HLabel 8400 3000 2    50   Input ~ 0
CT_6_L
Text HLabel 8400 3150 2    50   Input ~ 0
CT_7_K
Text HLabel 8400 3250 2    50   Input ~ 0
CT_7_L
$Comp
L power:+5V #PWR01
U 1 1 5FCC2A2A
P 3500 2500
F 0 "#PWR01" H 3500 2350 50  0001 C CNN
F 1 "+5V" H 3515 2673 50  0000 C CNN
F 2 "" H 3500 2500 50  0001 C CNN
F 3 "" H 3500 2500 50  0001 C CNN
	1    3500 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR03
U 1 1 5FCC174E
P 3500 3350
F 0 "#PWR03" H 3500 3200 50  0001 C CNN
F 1 "+24V" H 3515 3523 50  0000 C CNN
F 2 "" H 3500 3350 50  0001 C CNN
F 3 "" H 3500 3350 50  0001 C CNN
	1    3500 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR02
U 1 1 5FCC53A3
P 3500 2800
F 0 "#PWR02" H 3500 2550 50  0001 C CNN
F 1 "GNDD" H 3504 2645 50  0000 C CNN
F 2 "" H 3500 2800 50  0001 C CNN
F 3 "" H 3500 2800 50  0001 C CNN
	1    3500 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR04
U 1 1 5FCC55C8
P 3500 3650
F 0 "#PWR04" H 3500 3400 50  0001 C CNN
F 1 "GNDA" H 3505 3477 50  0000 C CNN
F 2 "" H 3500 3650 50  0001 C CNN
F 3 "" H 3500 3650 50  0001 C CNN
	1    3500 3650
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:5V_Regulator-gshp_lib U18
U 1 1 5FCBC350
P 2900 2400
AR Path="/5FC493CE/5FCBC350" Ref="U18"  Part="1" 
AR Path="/5FC493CE/5FCBC350" Ref="U18"  Part="1" 
F 0 "U18" H 2950 2515 50  0000 C CNN
F 1 "5V_Regulator" H 2950 2424 50  0000 C CNN
F 2 "" H 2900 2400 50  0001 C CNN
F 3 "" H 2900 2400 50  0001 C CNN
	1    2900 2400
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:24V_Regulator-gshp_lib U19
U 1 1 5FCBCCF1
P 2900 3250
AR Path="/5FC493CE/5FCBCCF1" Ref="U19"  Part="1" 
AR Path="/5FCBCCF1" Ref="U19"  Part="1" 
AR Path="/5FC493CE/5FCBCCF1" Ref="U19"  Part="1" 
F 0 "U19" H 2950 3365 50  0000 C CNN
F 1 "24V_Regulator" H 2950 3274 50  0000 C CNN
F 2 "" H 2900 3250 50  0001 C CNN
F 3 "" H 2900 3250 50  0001 C CNN
	1    2900 3250
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:TIM-94N-4CH-gshp_lib U20
U 1 1 5FC8233E
P 6900 4250
AR Path="/5FC493CE/5FC8233E" Ref="U20"  Part="1" 
AR Path="/5FC493CE/5FC8233E" Ref="U20"  Part="1" 
F 0 "U20" H 7500 4600 50  0000 C CNN
F 1 "TIM-94N-4CH" H 7000 2850 50  0000 C CNN
F 2 "" H 6900 4250 50  0001 C CNN
F 3 "" H 6900 4250 50  0001 C CNN
	1    6900 4250
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:TIM-94N-4CH-gshp_lib U?
U 1 1 62473C6C
P 9400 4250
AR Path="/5FC493CE/62473C6C" Ref="U?"  Part="1" 
AR Path="/5FC493CE/62473C6C" Ref="U?"  Part="1" 
F 0 "U?" H 10000 4600 50  0000 C CNN
F 1 "TIM-94N-4CH" H 9500 2850 50  0000 C CNN
F 2 "" H 9400 4250 50  0001 C CNN
F 3 "" H 9400 4250 50  0001 C CNN
	1    9400 4250
	1    0    0    -1  
$EndComp
$Comp
L gshp_power_controller_schematic-rescue:KVH_JK23663-gshp_lib U21
U 1 1 5FC7B5D2
P 7150 1900
AR Path="/5FC493CE/5FC7B5D2" Ref="U21"  Part="1" 
AR Path="/5FC493CE/5FC7B5D2" Ref="U21"  Part="1" 
F 0 "U21" H 7500 2450 50  0000 C CNN
F 1 "KVH_JK23663" H 7000 400 50  0000 C CNN
F 2 "" H 7250 1400 50  0001 C CNN
F 3 "" H 7250 1400 50  0001 C CNN
	1    7150 1900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
